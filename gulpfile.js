
var gulp						= require('gulp'),
	sass						= require('gulp-sass'),
	watch						= require('gulp-watch');

var pkg							= require('./package.json');

gulp.task('styles', function()
{
	gulp.src('./test/*.scss')
		.pipe(sass
			(
				{
					outputStyle		: "expanded",
					errLogToConsole	: true,
					indentType 		: 'tab',
					indentWidth		: 1,
				}
			)
		)
		.pipe(gulp.dest('./dist'));
});

gulp.task('styles:watch', function()
	{
		watch(['./*.scss', 'test/*.scss'], function(vinyl)
		{
			gulp.start.apply(gulp, ['styles']);
		}
	);
});

gulp.task('default', ['styles', 'styles:watch']);

